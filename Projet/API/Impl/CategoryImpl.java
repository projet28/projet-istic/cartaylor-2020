package Impl;
import Interface.*;

	public class CategoryImpl implements Category, Comparable<Category> {
		private String name;
		
		public CategoryImpl(String s) {
			name = s;
		}
		
		@Override
		public String getName() {
			return name;
		}

		@Override
		public int compareTo(Category o) {
			return name.compareTo(o.getName());
		}
	}
