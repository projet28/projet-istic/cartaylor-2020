package Impl;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import Interface.*;


	public class ConfiguratorImpl implements Configurator {
		private Configuration configuration;
		private CompatibilityChecker cc;
		private Map<Category,Set<PartType>> parts;
		
		
		public ConfiguratorImpl(CompatibilityChecker cc, Map<Category,Set<PartType>> parts) {
			this.cc = cc;
			configuration = new ConfigurationImpl(cc, parts);
			this.parts = parts;
		}
		
		
		@Override
		public Set<Category> getCategories() {
			return Collections.unmodifiableSet(parts.keySet());
		}

		@Override
		public Set<PartType> getVariants(Category category) {
			if(category != null) return Collections.unmodifiableSet(parts.get(category));
			return null;
		}

		@Override
		public Configuration getConfiguration() {
			return configuration;
		}

		@Override
		public CompatibilityChecker getCompatibilityChecker() {
			return cc;
		}



}
