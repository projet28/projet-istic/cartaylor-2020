package Impl;

import Interface.*;

	public class PartTypeImpl implements PartType, Comparable<PartType> {
		private String name;
		private Category category;
		
		public PartTypeImpl(String s, Category c1) {
			name =s;
			category = c1;
		}
		

		@Override
		public String getName() {
			return name;
		}


		@Override
		public Category getCategory() {
			return category;
		}


		@Override
		public int compareTo(PartType o) {
			return name.compareTo(o.getName());
		}
}
