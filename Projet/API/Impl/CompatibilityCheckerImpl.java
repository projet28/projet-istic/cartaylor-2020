package Impl;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import Interface.*;

	public class CompatibilityCheckerImpl implements CompatibilityChecker {
		protected Map<PartType, Set<PartType>> incompatibilities;
		protected Map<PartType, Set<PartType>> requirements;
		
		public CompatibilityCheckerImpl() {
			incompatibilities = new TreeMap<PartType, Set<PartType>>();
 			requirements = new TreeMap<PartType, Set<PartType>>();
		}
		
		@Override
		public Set<PartType> getIncompatibilities(PartType reference) {
			return incompatibilities.get(reference);
		}

		@Override
		public Set<PartType> getRequirements(PartType reference) {
			return requirements.get(reference);
		}

	}
