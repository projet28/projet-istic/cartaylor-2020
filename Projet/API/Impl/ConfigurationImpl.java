package Impl;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import Interface.*;

	public class ConfigurationImpl implements Configuration {
		private Set<PartType> partTypes;
		private CompatibilityChecker cc;
		private Map<Category,Set<PartType>> parts;
		
		public ConfigurationImpl(CompatibilityChecker cc, Map<Category,Set<PartType>> parts) {
			partTypes = new TreeSet<PartType>();
			this.cc = cc;
			this.parts = parts;
		}
		
		
		@Override
		public boolean isValid() {
			for(PartType p: partTypes) {
				if(cc.getRequirements(p) != null && !partTypes.containsAll(cc.getRequirements(p)))
					return false;
				if(cc.getIncompatibilities(p) != null) {
					for(PartType p2: cc.getIncompatibilities(p)) {
						if(partTypes.contains(p2)) return false;
					}
				}
			}
			return true;
		}


		@Override
		public boolean isComplete() {
			return partTypes.size() == parts.size();
		}

		@Override
		public Set<PartType> getSelectedParts() {
			return Collections.unmodifiableSet(partTypes);
		}

		@Override
		public void selectPart(PartType chosenPart) {
			Objects.requireNonNull(chosenPart);
			PartType cat = null;
			for(PartType p: partTypes) {
				if(p.getCategory() == chosenPart.getCategory())
					cat = p;
			}
			if(cat != null) partTypes.remove(cat);
			partTypes.add(chosenPart);
		}

		@Override
		public PartType getSelectionForCategory(Category category) {
			Objects.requireNonNull(category);
			for(PartType p : partTypes) {
				if (p.getCategory() == category)
					return p;
			}
			return null;
		}

		@Override
		public void unselectPartType(Category categoryToClear) {
			Objects.requireNonNull(categoryToClear);
			PartType p = getSelectionForCategory(categoryToClear);
			if(p != null) partTypes.remove(p);
		}

		@Override
		public void clear() {
			partTypes.clear();			
		}
	}