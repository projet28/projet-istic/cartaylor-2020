package Impl;
import java.util.Objects;
import java.util.Set;

import Interface.*;

	public class CompatibilityManagerImpl extends CompatibilityCheckerImpl implements CompatibilityManager {
		
		public CompatibilityManagerImpl() {
			super();
		}
		
		
		@Override
		public Set<PartType> getIncompatibilities(PartType reference) {
			return super.getIncompatibilities(reference);
		}

		@Override
		public Set<PartType> getRequirements(PartType reference) {
			return super.getRequirements(reference);
		}

		@Override
		public void addIncompatibilities(PartType reference, Set<PartType> target) throws ConflictingRuleException {
			Objects.requireNonNull(reference);
			Objects.requireNonNull(target);
			if(target.contains(reference)) {
				throw new ConflictingRuleException("La reference ne peut pas �tre incompatible � elle m�me");
			}
			Set<PartType> req = getRequirements(reference);
			if(req != null) {
				for(PartType ptype : target) {
					if(req.contains(ptype)) {
						throw new ConflictingRuleException("La reference a deja des requirements dans target");
					}
				}
			}
			if(incompatibilities.containsKey(reference)){
				incompatibilities.get(reference).addAll(target);
			}else {
				incompatibilities.put(reference, target);
			}
		}

		@Override
		public void removeIncompatibility(PartType reference, PartType target) {
			Objects.requireNonNull(reference);
			Objects.requireNonNull(target);
			this.incompatibilities.get(reference).remove(target);
			if(this.incompatibilities.get(reference).isEmpty())
				this.incompatibilities.remove(reference);
		}

		@Override
		public void addRequirements(PartType reference, Set<PartType> target) throws ConflictingRuleException {
			Objects.requireNonNull(reference);
			Objects.requireNonNull(target);
			if(target.contains(reference)) {
				throw new ConflictingRuleException("La reference ne peut pas �tre requis � elle m�me");
			}
			Set<PartType> inc = getIncompatibilities(reference);
			if(inc != null) {
				for(PartType ptype : target) {
					if(inc.contains(ptype)) {
						throw new ConflictingRuleException("La reference a deja des requirements dans target");
					}
				}
			}
			if(requirements.containsKey(reference)){
				requirements.get(reference).addAll(target);
			}else {
				requirements.put(reference, target);
			}	
		}

		@Override
		public void removeRequirement(PartType reference, PartType target) {
			Objects.requireNonNull(reference);
			Objects.requireNonNull(target);
			this.requirements.get(reference).remove(target);
			if(this.requirements.get(reference).isEmpty())
				this.requirements.remove(reference);
		}
	}
