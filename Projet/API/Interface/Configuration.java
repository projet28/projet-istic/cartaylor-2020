package Interface;

import java.util.Set;

/**
 * @author rozé et rabe
 * <p>
 * Interface who manage the different PartType of the configuration
 */
public interface Configuration {

	/**
	 * 
	 * @return True if the configuration is valid (check compatibility for each PartType)
	 */
    boolean isValid();

    /**
     * 
     * @return True if the configuration have the 4 differents category
     */
    boolean isComplete();

    /**
     * 
     * @return all the PartType of the configuration
     */
    Set<PartType> getSelectedParts();

    /**
     * 
     * @param chosenPart add the chosenPart in the configuration
     * if the category of the chosenPart already exist, it replace the former partType by the chosenPart
     */
    void selectPart(PartType chosenPart);

    /**
     * @param category the choosen category for the selection 
     * @return the PartType of the selected category
     */
    PartType getSelectionForCategory(Category category);

    /**
     * @param categoryToClear the choosen category to remove the PartType
     */
    void unselectPartType(Category categoryToClear);

    /**
     * remove all the PartType
     */
    void clear();

}
