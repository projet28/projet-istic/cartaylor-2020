package Interface;

import java.util.Set;

/**
 * @author roz� et rabe
 * <p>
 * Interface who check the compabatibility between a partType and a set of other PartType
 * They can be Incompatibilities OR Requirements
 */
public interface CompatibilityChecker {

	/**
	 * 
	 * @param reference The reference to get Incompatibility
	 * @return the set of Incompatible PartType
	 */
    Set<PartType> getIncompatibilities(PartType reference);

    
    /**
	 * 
	 * @param reference The reference to get Requirements
	 * @return the set of Requiere PartType
	 */
    Set<PartType> getRequirements(PartType reference);

}
