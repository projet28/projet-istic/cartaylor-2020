package Interface;
/**
 * @author roz� et rabe
 * <p>
 * Interface of the PartType
 */
public interface PartType {

	/**
	 * 
	 * @return the name of the PartType
	 */
    String getName();
    
    /**
     * 
     * @return the category of the partType
     */
    Category getCategory();
}
