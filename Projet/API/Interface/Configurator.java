package Interface;

import java.util.Set;

/**
 * @author rozé et rabe
 * <p>
 * Interface who contain all the other implementation
 */
public interface Configurator {

	/**
	 * 
	 * @return all the different possibility of categories
	 */
    Set<Category> getCategories();

    /**
     * 
     * @param category the category to check
     * @return all the PartType of the choosen category
     */
    Set<PartType> getVariants(Category category);

    /**
     * 
     * @return the configuration of the car
     */
    Configuration getConfiguration();

    /**
     * 
     * @return the CompatibilityChecker for the configuration
     */
    CompatibilityChecker getCompatibilityChecker();

}
