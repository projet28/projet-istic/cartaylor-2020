package Interface;

import java.util.Set;

import Impl.ConflictingRuleException;

/**
 * @author roz� et rabe
 * <p>
 * Interface who Manage the compatibility (add or remove Incompatibilities and Requirements)
 */
public interface CompatibilityManager extends CompatibilityChecker {

	/**
	 * 
	 * @param reference the reference to add Incompatibilities
	 * @param target the new Incompatibilities to add
	 * @throws ConflictingRuleException the target can't be in the reference's Requirements
	 */
    void addIncompatibilities(PartType reference, Set<PartType> target) throws ConflictingRuleException;

    /**
	 * 
	 * @param reference the reference to remove Incompatibilitie
	 * @param target the Incompatibilitie to remove
	 */
    void removeIncompatibility(PartType reference, PartType target);

    /**
	 * 
	 * @param reference the reference to add Requirements
	 * @param target the new Requirements to add
	 * @throws ConflictingRuleException the target can't be in the reference's Incompatibilities
	 */
    void addRequirements(PartType reference, Set<PartType> target) throws ConflictingRuleException;

    /**
	 * 
	 * @param reference the reference to remove Requirement
	 * @param target the new Requirement to remove
	 */
    void removeRequirement(PartType reference, PartType target);

}
