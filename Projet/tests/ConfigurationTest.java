import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import Impl.*;
import Interface.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class ConfigurationTest {

	CompatibilityChecker cc = new CompatibilityManagerImpl();
	Map<Category, Set<PartType>> part = new TreeMap<Category, Set<PartType>>();
	Configuration conf = new ConfigurationImpl(cc, part);
	Category c1 = new CategoryImpl("moteur");
	Category c2 = new CategoryImpl("transition");
	Category c3 = new CategoryImpl("exterieur");
	Category c4 = new CategoryImpl("interieur");
	PartType p0 = new PartTypeImpl("v6", c1);
	
	@Test
	@DisplayName("verifie si les ajouts dans la configuration marchent bien")
	public void testSelectPart() throws ConflictingRuleException{
		PartType p1 = new PartTypeImpl("v10", c1);
		Set<PartType> s1 = new TreeSet<PartType>();
		s1.add(p1);
		part.put(c1, s1);
		conf.selectPart(p1);
		assertTrue(!conf.getSelectedParts().isEmpty());
		
		PartType p2 = new PartTypeImpl("t300_20", c2);
		Set<PartType> s2 = new TreeSet<PartType>();
		s2.add(p2);
		part.put(c2, s2);
		conf.selectPart(p2);
		assertTrue(conf.getSelectedParts().size() == 2);
		
		PartType p3 = new PartTypeImpl("lambo", c3);
		Set<PartType> s3 = new TreeSet<PartType>();
		s1.add(p3);
		part.put(c3, s3);
		conf.selectPart(p3);
		assertTrue(conf.getSelectedParts().size() == 3);
		
		Set<PartType> inc = new TreeSet<PartType>();
		inc.add(p2);
		((CompatibilityManagerImpl) cc).addIncompatibilities(p1, inc);
		assertFalse(((CompatibilityManagerImpl) cc).getIncompatibilities(p1).isEmpty());
		
		
		Set<PartType> req = new TreeSet<PartType>();
		req.add(p0);
		((CompatibilityManagerImpl) cc).addRequirements(p3, req);
		assertFalse(((CompatibilityManagerImpl) cc).getRequirements(p3).isEmpty());
	}
	
	@Test
	@DisplayName("tests pour la fonction isComplete")
	public void testComplete() throws ConflictingRuleException {
		testSelectPart();
		
		PartType p1 = new PartTypeImpl("confort", c4);
		Set<PartType> s1 = new TreeSet<PartType>();
		s1.add(p1);
		part.put(c4, s1);
		
		assertFalse(conf.isComplete());
		conf.selectPart(p1);
		assertTrue(conf.isComplete());
		
		PartType p2 = new PartTypeImpl("sport", c4);
		Set<PartType> s2 = new TreeSet<PartType>();
		s2.add(p2);
		part.get(c4).addAll(s2);
		conf.selectPart(p2);
		assertTrue(conf.isComplete());
		
		conf.unselectPartType(c4);
		assertFalse(conf.isComplete());
	}
	
	@Test
	@DisplayName("tests pour la fonction isValid")
	public void testisValid() throws ConflictingRuleException{
		testComplete();
		
		PartType p1 = new PartTypeImpl("confort", c4);
		conf.selectPart(p1);
		assertFalse(conf.isValid());
		
		((CompatibilityManagerImpl) cc).removeIncompatibility(conf.getSelectionForCategory(c1), conf.getSelectionForCategory(c2));
		assertTrue(((CompatibilityManagerImpl) cc).getIncompatibilities(conf.getSelectionForCategory(c1)) == null);
		assertFalse(conf.isValid());
		
		((CompatibilityManagerImpl) cc).removeRequirement(conf.getSelectionForCategory(c3), p0);
		assertTrue(((CompatibilityManagerImpl) cc).getRequirements(conf.getSelectionForCategory(c3)) == null);
		assertTrue(conf.isValid());
		
		Set<PartType> req = new TreeSet<PartType>();
		req.add(conf.getSelectionForCategory(c2));
		
		((CompatibilityManagerImpl) cc).addIncompatibilities(p1, req);
		assertFalse(((CompatibilityManagerImpl) cc).getIncompatibilities(p1).isEmpty());
		
		try {
			((CompatibilityManagerImpl) cc).addRequirements(p1, req);
		} catch (ConflictingRuleException e) {
			assertTrue(((CompatibilityManagerImpl) cc).getRequirements(p1) == null);
		}
	}
	
	@Test
	@DisplayName("test pour la fonction clear")
	public void TestClear() throws ConflictingRuleException {
		testisValid();
		assertFalse(conf.getSelectedParts().isEmpty());
		
		conf.clear();
		assertTrue(conf.getSelectedParts().isEmpty());
	}
}
