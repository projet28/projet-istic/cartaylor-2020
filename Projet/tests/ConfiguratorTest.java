import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import Impl.*;
import Interface.*;

import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.junit.Assert.assertFalse;

public class ConfiguratorTest {
	
	Map<Category, Set<PartType>> part = new TreeMap<Category, Set<PartType>>();
	
	Category c1 = new CategoryImpl("moteur");
	Category c2 = new CategoryImpl("transition");
	Category c3 = new CategoryImpl("exterieur");
	Category c4 = new CategoryImpl("interieur");
	
	PartType p11 = new PartTypeImpl("v10", c1);
	PartType p12 = new PartTypeImpl("v12", c1);
	PartType p21 = new PartTypeImpl("T200", c2);
	PartType p22 = new PartTypeImpl("T250", c2);
	PartType p31 = new PartTypeImpl("ferrari", c3);
	PartType p32 = new PartTypeImpl("lambo", c3);
	PartType p41 = new PartTypeImpl("confort", c4);
	PartType p42 = new PartTypeImpl("sport", c4);
	
	Set<PartType> moteur = new TreeSet<PartType>();
	Set<PartType> transmission = new TreeSet<PartType>();
	Set<PartType> interrieur = new TreeSet<PartType>();
	Set<PartType> exterieur = new TreeSet<PartType>();
	
	CompatibilityChecker cc = new CompatibilityManagerImpl();

	Configurator conf = new ConfiguratorImpl(cc, part);
	
	PartType p0 = new PartTypeImpl("v6", c1);
	
	@Test
	@DisplayName("verifie si les ajouts dans le configurator marchent bien")
	public void testPartType() {
		assertTrue(conf.getCategories().isEmpty());
		
		moteur.add(p11);
		moteur.add(p12);
		transmission.add(p21);
		transmission.add(p22);
		interrieur.add(p31);
		interrieur.add(p32);
		exterieur.add(p41);
		exterieur.add(p42);
		
		part.put(c1, moteur);
		part.put(c2, transmission);
		part.put(c3, interrieur);
		part.put(c4, exterieur);
		
		assertFalse(conf.getCategories().isEmpty());
		assertFalse(conf.getVariants(c1).isEmpty());
		assertFalse(conf.getVariants(c2).isEmpty());
		assertFalse(conf.getVariants(c3).isEmpty());
		assertFalse(conf.getVariants(c4).isEmpty());
	}
	
	@Test
	@DisplayName("verifie si les ajouts dans la configuration marchent bien")
	public void testSelectPart() throws ConflictingRuleException{
		testPartType();
		conf.getConfiguration().selectPart(p11);
		assertFalse(conf.getConfiguration().getSelectedParts().isEmpty());
		
		conf.getConfiguration().selectPart(p21);
		assertTrue(conf.getConfiguration().getSelectedParts().size() == 2);
		
		conf.getConfiguration().selectPart(p31);
		assertTrue(conf.getConfiguration().getSelectedParts().size() == 3);
		
		Set<PartType> inc = new TreeSet<PartType>();
		inc.add(p21);
		((CompatibilityManagerImpl) conf.getCompatibilityChecker()).addIncompatibilities(p11, inc);
		assertFalse(conf.getCompatibilityChecker().getIncompatibilities(p11).isEmpty());
		
		
		Set<PartType> req = new TreeSet<PartType>();
		req.add(p0);
		((CompatibilityManagerImpl) conf.getCompatibilityChecker()).addRequirements(p31, req);
		assertFalse(conf.getCompatibilityChecker().getRequirements(p31).isEmpty());
	}
	
	@Test
	@DisplayName("test pour la fonction isComplete")
	public void testComplete() throws ConflictingRuleException{
		testSelectPart();
		assertFalse(conf.getConfiguration().isComplete());
		
		conf.getConfiguration().selectPart(p41);
		assertTrue(conf.getConfiguration().isComplete());
		
		conf.getConfiguration().selectPart(p42);
		assertTrue(conf.getConfiguration().isComplete());
		
		conf.getConfiguration().unselectPartType(c4);
		assertFalse(conf.getConfiguration().isComplete());
	}
	
	@Test
	@DisplayName("test pour la fonction isValid")
	public void testisValid() throws ConflictingRuleException{
		testComplete();
		
		conf.getConfiguration().selectPart(p41);
		assertFalse(conf.getConfiguration().isValid());
		
		((CompatibilityManagerImpl) conf.getCompatibilityChecker()).removeIncompatibility(conf.getConfiguration().getSelectionForCategory(c1), conf.getConfiguration().getSelectionForCategory(c2));
		assertTrue( conf.getCompatibilityChecker().getIncompatibilities(conf.getConfiguration().getSelectionForCategory(c1)) == null);
		assertFalse(conf.getConfiguration().isValid());
		
		((CompatibilityManagerImpl) conf.getCompatibilityChecker()).removeRequirement(conf.getConfiguration().getSelectionForCategory(c3), p0);
		assertTrue(conf.getCompatibilityChecker().getRequirements(conf.getConfiguration().getSelectionForCategory(c3)) == null);
		assertTrue(conf.getConfiguration().isValid());
		
		Set<PartType> req = new TreeSet<PartType>();
		req.add(conf.getConfiguration().getSelectionForCategory(c2));
		
		((CompatibilityManagerImpl) conf.getCompatibilityChecker()).addIncompatibilities(p41, req);
		assertFalse(((CompatibilityManagerImpl) conf.getCompatibilityChecker()).getIncompatibilities(p41).isEmpty());
		
		try {
			((CompatibilityManagerImpl) conf.getCompatibilityChecker()).addRequirements(p41, req);
		} catch (ConflictingRuleException e) {
			assertTrue(((CompatibilityManagerImpl) conf.getCompatibilityChecker()).getRequirements(p41) == null);
		}
	}
	
	@Test
	@DisplayName("test pour la fonction clear")
	public void TestClear() throws ConflictingRuleException {
		testisValid();
		assertFalse(conf.getConfiguration().getSelectedParts().isEmpty());
		
		conf.getConfiguration().clear();
		assertTrue(conf.getConfiguration().getSelectedParts().isEmpty());
	}
}
